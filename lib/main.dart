import 'package:auth_app/services/google_sign_service.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Auth - App Google',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('AuthApp Google -Apple', style: TextStyle(color: Colors.black54),),
          elevation: 1,
          actions: [
            IconButton(
              icon: Icon(FontAwesomeIcons.doorOpen, color: Colors.black54,), 
              onPressed: () {
                GoogleSignInService.signOut();
              }
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                minWidth: double.infinity,
                height: 40,
                color: Colors.red,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                child: Row(
                  mainAxisAlignment:  MainAxisAlignment.center,
                  children: [
                    
                    Icon(FontAwesomeIcons.google,color: Colors.white,),
                    SizedBox(width: 10,),
                    Text('Sign in with Google',style: TextStyle(color: Colors.white, fontSize: 17))
                  ],
                ),
                onPressed: () {
                  GoogleSignInService.signInWithGoogle();
                }
              )
            ],
          ),
        )
      ),
    );
  }
}